package com.evdokimov.testtask.personCatalog.domain;

import com.evdokimov.testtask.titanMapper.annotation.*;
import com.thinkaurelius.titan.core.*;

/**
 *
 */
@Relationship(multiplicity = Multiplicity.MANY2ONE)
public class WorkFor {

    @StartNode
    private Person person;

    @EndNode
    private Company company;

    @Property
    Long startDate;

    public Person getPerson() {
        return person;
    }

    public WorkFor setPerson(Person person) {
        this.person = person;

        return this;
    }

    public Company getCompany() {
        return company;
    }

    public WorkFor setCompany(Company company) {
        this.company = company;

        return this;
    }

    public Long getStartDate() {
        return startDate;
    }

    public WorkFor setStartDate(Long startDate) {
        this.startDate = startDate;

        return this;
    }
}

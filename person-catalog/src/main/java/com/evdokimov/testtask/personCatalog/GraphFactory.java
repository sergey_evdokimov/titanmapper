package com.evdokimov.testtask.personCatalog;

import com.thinkaurelius.titan.core.*;
import org.apache.commons.configuration.*;

import java.io.*;

/**
 *
 */
public class GraphFactory {

    private String dbPath;

    public String getDbPath() {
        return dbPath;
    }

    public void setDbPath(String dbPath) {
        this.dbPath = dbPath;
    }

    public TitanGraph createHBaseGraph() {
        Configuration conf = new BaseConfiguration();
        conf.setProperty("storage.backend","hbase");

        return TitanFactory.open(conf);
    }

    public TitanGraph createBerkeleyjeGraph() {
        if (dbPath == null)
            throw new IllegalArgumentException("Data base path is not provided.");

        TitanFactory.Builder config = TitanFactory.build();
        config.set("storage.backend", "berkeleyje");
        config.set("storage.directory", dbPath);
        config.set("index."+ "search" +".backend","elasticsearch");
        config.set("index." + "search" + ".directory", dbPath + File.separator + "es");
        config.set("index."+ "search" +".elasticsearch.local-mode",true);
        config.set("index."+ "search" +".elasticsearch.client-only",false);

        return config.open();
    }

}

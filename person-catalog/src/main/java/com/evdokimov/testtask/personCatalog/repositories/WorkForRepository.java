package com.evdokimov.testtask.personCatalog.repositories;

import com.evdokimov.testtask.personCatalog.domain.*;
import com.evdokimov.testtask.titanMapper.*;

/**
 *
 */
public interface WorkForRepository extends TitanRelationRepository<WorkFor, Person, Company> {

}

package com.evdokimov.testtask.personCatalog.domain;

import com.evdokimov.testtask.titanMapper.annotation.*;

/**
 *
 */
@Node
public class Company {

    @Property
    private String name;

    public String getName() {
        return name;
    }

    public Company setName(String name) {
        this.name = name;

        return this;
    }
}

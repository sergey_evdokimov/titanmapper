package com.evdokimov.testtask.personCatalog.repositories;

import com.evdokimov.testtask.personCatalog.domain.*;
import com.evdokimov.testtask.titanMapper.*;

/**
 *
 */
public interface CompanyRepository extends TitanNodeRepository<Company> {

    Company findByName(String name);

    Iterable<Company> findAllByName(String name);

}

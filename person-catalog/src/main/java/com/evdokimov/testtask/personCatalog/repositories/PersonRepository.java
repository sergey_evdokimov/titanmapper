package com.evdokimov.testtask.personCatalog.repositories;

import com.evdokimov.testtask.personCatalog.domain.*;
import com.evdokimov.testtask.titanMapper.*;

import java.util.*;

/**
 *
 */
public interface PersonRepository extends TitanNodeRepository<Person> {

    Person findByName(String name);

    List<Person> findAllByName(String name);

}

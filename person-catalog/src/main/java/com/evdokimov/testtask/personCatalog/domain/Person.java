package com.evdokimov.testtask.personCatalog.domain;

import com.evdokimov.testtask.titanMapper.annotation.*;

import java.io.*;

/**
 *
 */
@Node
public class Person implements Serializable {

//    private long id;

    @Property
    private String name;

    @Property
    private int old;

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;

        return this;
    }

    public int getOld() {
        return old;
    }

    public Person setOld(int old) {
        this.old = old;

        return this;
    }

//    @Override public long getId() {
//        return id;
//    }

//    @Override public void setId(long id) {
//        this.id = id;
//    }
}

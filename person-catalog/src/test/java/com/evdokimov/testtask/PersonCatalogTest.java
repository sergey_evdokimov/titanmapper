package com.evdokimov.testtask;

import com.evdokimov.testtask.personCatalog.domain.*;
import com.evdokimov.testtask.personCatalog.repositories.*;
import com.evdokimov.testtask.titanMapper.*;
import com.google.common.collect.*;
import com.thinkaurelius.titan.core.*;
import com.tinkerpop.blueprints.*;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.util.*;

import static org.junit.Assert.*;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/spring/person-catalog.xml")
public class PersonCatalogTest {
    @Autowired
    private TitanManager titanManager;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private WorkForRepository workForRepository;

    @Before
    public void updateGraphSchema() {
        titanManager.updateGraphSchema(Person.class, Company.class, WorkFor.class);
    }

    @Before
    public void cleanDataBase() {
        TitanGraph graph = titanManager.getGraph();

        for (Edge edge : graph.getEdges()) {
            graph.removeEdge(edge);
        }

        for (Vertex vertex : graph.getVertices()) {
            graph.removeVertex(vertex);
        }

        graph.commit();
    }

    @After
    public void rollbackTx() {
        titanManager.rollback();
    }

    @Test
    public void testSaveLoad() {
        Person p1 = new Person().setName("Roma").setOld(30);
        Company c1 = new Company().setName("Behavox");

        WorkFor w1 = new WorkFor().setCompany(c1).setPerson(p1).setStartDate(new Date().getTime());

        workForRepository.save(w1);

        assertTrue(personRepository.isPersistent(p1));
        assertTrue(companyRepository.isPersistent(c1));
        assertNotNull(personRepository.getEntityId(p1));
        assertNotNull(companyRepository.getEntityId(c1));

        assertSame(p1, personRepository.findByName("Roma"));
        assertSame(c1, companyRepository.findByName("Behavox"));

        RelationId wId = workForRepository.getEntityId(w1);

        workForRepository.commit();

        WorkFor readW = workForRepository.findOne(wId);

        assertEquals("Roma", readW.getPerson().getName());
        assertEquals(30, readW.getPerson().getOld());
        assertEquals("Behavox", readW.getCompany().getName());
    }

    @Test
    public void testNavigateRelation() {
        Company c1 = new Company().setName("Behavox");

        Person p1 = new Person().setName("Roma");
        Person p2 = new Person().setName("Ivan");

        WorkFor w1 = new WorkFor().setCompany(c1).setPerson(p1);
        WorkFor w2 = new WorkFor().setCompany(c1).setPerson(p2);

        workForRepository.save(Arrays.asList(w1, w2));

        assertEquals(Collections.singletonList(w1), Lists.newArrayList(workForRepository.getOuts(p1)));
        assertEquals(Collections.singletonList(w2), Lists.newArrayList(workForRepository.getOuts(p2)));

        assertEquals(ImmutableSet.of(w1, w2), Sets.newHashSet(workForRepository.getIn(c1)));
    }

    @Test
    public void testTransaction() {
        Person p = new Person().setName("Ivan").setOld(24);

        personRepository.save(p);

        assert personRepository.findOne(personRepository.getEntityId(p)) == p;
        assert personRepository.findByName("Ivan") == p;

        personRepository.executeInTransaction(new TitanRunnable() {
            @Override public void run() {
                assert personRepository.findByName("Ivan") == null; // it's another transaction.
            }
        });

        personRepository.commit();

        p = personRepository.findByName("Ivan");

        p.setName("Vasia");

        personRepository.save(p);

        assert personRepository.findByName("Ivan") == null;
        assert personRepository.findByName("Vasia") != null;

        personRepository.executeInTransaction(new TitanRunnable() {
            @Override public void run() {
                assert personRepository.findByName("Ivan").getName().equals("Ivan");
            }
        });

        personRepository.rollback();

        assert personRepository.findByName("Ivan") != null;
    }

}

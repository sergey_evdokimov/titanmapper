package com.evdokimov.testtask.titanMapper;

import org.jetbrains.annotations.*;
import org.springframework.data.repository.*;

import java.io.*;

/**
 *
 */
public interface TitanRepositoryBase<T, ID extends Serializable> extends CrudRepository<T, ID> {

    void commit();

    void rollback();

    ID getEntityId(@NotNull T entity);

    boolean isPersistent(@NotNull T entity);

    <R> R executeInTransaction(@NotNull TitanCallable<R> callable);
}

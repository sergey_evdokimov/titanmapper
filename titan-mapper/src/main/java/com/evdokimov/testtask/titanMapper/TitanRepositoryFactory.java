package com.evdokimov.testtask.titanMapper;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.repository.*;
import org.springframework.data.repository.core.support.*;

import java.io.*;

/**
 *
 */
public class TitanRepositoryFactory<T extends Repository<S, ID>, S, ID extends Serializable>
    extends RepositoryFactoryBeanSupport<T, S, ID> {

    @Autowired
    private TitanManager titanManager;

    @Override
    protected RepositoryFactorySupport createRepositoryFactory() {
        return new TitanRepositoryFactorySupport(titanManager);
    }

    public TitanManager getTitanManager() {
        return titanManager;
    }

    public void setTitanManager(TitanManager titanManager) {
        this.titanManager = titanManager;
    }
}

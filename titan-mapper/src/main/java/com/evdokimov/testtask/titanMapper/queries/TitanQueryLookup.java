package com.evdokimov.testtask.titanMapper.queries;

import com.evdokimov.testtask.titanMapper.*;
import com.evdokimov.testtask.titanMapper.annotation.*;
import org.jetbrains.annotations.*;
import org.springframework.data.mapping.model.*;
import org.springframework.data.repository.core.*;
import org.springframework.data.repository.query.*;

import java.lang.reflect.*;
import java.util.regex.*;

/**
 *
 */
public class TitanQueryLookup implements QueryLookupStrategy {

    private static final Pattern METHOD_NAME_PATTERN = Pattern.compile("(findBy|findAllBy)(?<prop>[A-Z][\\w+]*)");

    private final TitanManager titanManager;

    public TitanQueryLookup(@NotNull TitanManager manager) {
        titanManager = manager;
    }

    @Override
    public RepositoryQuery resolveQuery(Method method, RepositoryMetadata metadata, NamedQueries namedQueries) {
        Matcher matcher = METHOD_NAME_PATTERN.matcher(method.getName());

        if (!matcher.matches())
            throw new MappingException("Invalid method name: " + method.getName());

        QueryMethod queryMethod = new QueryMethod(method, metadata);

        String propertyName = decapitalize(matcher.group("prop"));

        switch (matcher.group(1)) {
            case "findBy":
                if (method.getParameterCount() != 1)
                    throw new MappingException("Method findBy* must has exactly one parameter.");

                if (!metadata.getDomainType().isAnnotationPresent(Node.class))
                    throw new MappingException("Method findBy* can be on Node repositories only.");

                return new QueryFindByOneProperty(titanManager, queryMethod, metadata.getDomainType(), propertyName);

            case "findAllBy":
                if (method.getParameterCount() != 1)
                    throw new MappingException("Method findAllBy* must has exactly one parameter.");

                if (!metadata.getDomainType().isAnnotationPresent(Node.class))
                    throw new MappingException("Method findAllBy* can be on Node repositories only.");

                return new QueryFindAllByOneProperty(titanManager, queryMethod, metadata.getDomainType(), propertyName);

            default:
                throw new InternalError();
        }
    }

    private static String decapitalize(String s) {
        if (s.isEmpty())
            return s;

        return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }
}

package com.evdokimov.testtask.titanMapper.config;

import org.springframework.beans.factory.xml.*;
import org.springframework.data.repository.config.*;

/**
 *
 */
public class TitanNamespaceHandler extends NamespaceHandlerSupport {
    @Override
    public void init() {
        RepositoryConfigurationExtension extension = new TitanRepositoryExtension();
        RepositoryBeanDefinitionParser repositoryBeanDefinitionParser = new RepositoryBeanDefinitionParser(extension);

        registerBeanDefinitionParser("repositories", repositoryBeanDefinitionParser);
    }
}

package com.evdokimov.testtask.titanMapper.annotation;

import org.springframework.data.annotation.*;

import java.lang.annotation.*;

/**
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@Persistent
public @interface Node {

}

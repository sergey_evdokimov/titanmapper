package com.evdokimov.testtask.titanMapper;

/**
 *
 */
public interface TitanCallable<R> {

    R execute();

}

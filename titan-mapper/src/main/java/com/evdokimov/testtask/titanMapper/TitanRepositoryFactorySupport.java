package com.evdokimov.testtask.titanMapper;

import com.evdokimov.testtask.titanMapper.annotation.*;
import com.evdokimov.testtask.titanMapper.queries.*;
import org.jetbrains.annotations.*;
import org.springframework.data.mapping.model.*;
import org.springframework.data.repository.core.*;
import org.springframework.data.repository.core.support.*;
import org.springframework.data.repository.query.*;

import java.io.*;

/**
 *
 */
public class TitanRepositoryFactorySupport extends RepositoryFactorySupport {

    private final TitanManager titanManager;

    public TitanRepositoryFactorySupport(@NotNull TitanManager titanManager) {
        this.titanManager = titanManager;
    }

    @Override public <T, ID extends Serializable> EntityInformation<T, ID> getEntityInformation(Class<T> domainClass) {
        if (domainClass.getAnnotation(Node.class) != null)
            return new NodeEntityInformation(domainClass);

        if (domainClass.getAnnotation(Relationship.class) != null)
            return new EdgeEntityInformation(domainClass);

        throw new MappingException("Domain class is not a Vertex or Relation");
    }

    @Override protected Object getTargetRepository(RepositoryMetadata metadata) {
        TitanPersistentEntity<?> persistentEntity = titanManager.getMappingContext().getPersistentEntity(metadata.getDomainType());

        if (persistentEntity.isVertex()) {
            if (!TitanNodeRepository.class.isAssignableFrom(metadata.getRepositoryInterface()))
                throw new MappingException("Node repository must implement " + TitanNodeRepository.class.getName());

            return new TitanRepositoryImpl<>(titanManager, persistentEntity);
        }
        else {
            if (!TitanRelationRepository.class.isAssignableFrom(metadata.getRepositoryInterface()))
                throw new MappingException("Relation repository must implement " + TitanRelationRepository.class.getName());

            return new TitanRelationRepositoryImpl<>(titanManager, persistentEntity);
        }
    }

    @Override protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        if (TitanRelationRepository.class.isAssignableFrom(metadata.getRepositoryInterface()))
            return TitanRelationRepositoryImpl.class;
        else
            return TitanRepositoryImpl.class;
    }

    @Override protected QueryLookupStrategy getQueryLookupStrategy(QueryLookupStrategy.Key key,
                                                                   EvaluationContextProvider evaluationContextProvider) {
        return new TitanQueryLookup(titanManager);
    }

    private static class NodeEntityInformation<T> extends AbstractEntityInformation<T, Long> {
        NodeEntityInformation(Class<T> domainClass) {
            super(domainClass);
        }

        @Override public Long getId(T entity) {
            throw new UnsupportedOperationException();
        }

        @Override public Class<Long> getIdType() {
            return Long.class;
        }
    }

    private static class EdgeEntityInformation<T> extends AbstractEntityInformation<T, RelationId> {
        EdgeEntityInformation(Class<T> domainClass) {
            super(domainClass);
        }

        @Override public RelationId getId(T entity) {
            throw new UnsupportedOperationException();
        }

        @Override public Class<RelationId> getIdType() {
            return RelationId.class;
        }
    }
}

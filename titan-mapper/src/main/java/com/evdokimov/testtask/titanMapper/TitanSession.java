package com.evdokimov.testtask.titanMapper;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.thinkaurelius.titan.core.*;
import com.thinkaurelius.titan.graphdb.relations.*;
import com.tinkerpop.blueprints.*;
import org.jetbrains.annotations.*;
import org.springframework.data.mapping.*;
import org.springframework.data.mapping.model.*;

import java.io.*;
import java.util.*;

/**
 *
 */
public class TitanSession {

    public static final String CLASS_PROPERTY_NAME = "class";

    private final TitanTransaction tx;

    private final TitanMappingContext mappingContext;

    private final Map<Element, Object> titanToDomain = new IdentityHashMap<>();

    private final Map<Object, Element> domainToTitan = new IdentityHashMap<>();

    public TitanSession(@NotNull TitanTransaction tx, @NotNull TitanMappingContext mappingContext) {
        this.tx = tx;
        this.mappingContext = mappingContext;
    }

    public <T> T toDomain(@NotNull Element titanElement, @NotNull TitanPersistentEntity<T> entity) {
        T domainInstance = (T)titanToDomain.get(titanElement);

        if (domainInstance == null) {
            try {
                domainInstance = entity.getPersistenceConstructor().getConstructor().newInstance();
            }
            catch (Exception e) {
                throw Throwables.propagate(e);
            }

            loadProperties(domainInstance, titanElement, entity);

            domainToTitan.put(domainInstance, titanElement);
            titanToDomain.put(titanElement, domainInstance);
        }

        return domainInstance;
    }

    public <T> Iterable<T> toDomain(Iterable<? extends Element> titanElements,
                                    @NotNull final TitanPersistentEntity<T> entity) {
        return Iterables.transform(titanElements, new Function<Element, T>() {
            @javax.annotation.Nullable @Override public T apply(Element element) {
                return toDomain(element, entity);
            }
        });
    }

    @Nullable
    public Element toTitanElement(Object domainInstance) {
        return domainToTitan.get(domainInstance);
    }

    public Element saveOrUpdate(@NotNull Object domainInstance, @NotNull TitanPersistentEntity<?> entity) {
        Element titanElement = domainToTitan.get(domainInstance);

        if (titanElement == null) {
            Serializable id;

            if (entity instanceof TitanEdgeEntity) {
                titanElement = createEdge(domainInstance, (TitanEdgeEntity<?>)entity);

                id = new RelationId((RelationIdentifier)titanElement.getId());
            }
            else {
                titanElement = tx.addVertex();

                titanElement.setProperty(CLASS_PROPERTY_NAME, entity.getLabel());

                id = (Long)titanElement.getId();
            }

            TitanPersistentProperty idProperty = entity.getIdProperty();
            if (idProperty != null) {
                entity.getPropertyAccessor(domainInstance).setProperty(idProperty, id);
            }

            titanToDomain.put(titanElement, domainInstance);
            domainToTitan.put(domainInstance, titanElement);
        }

        saveProperties(domainInstance, titanElement, entity);

        return titanElement;
    }

    private TitanElement createEdge(@NotNull final Object domainInstance, @NotNull final TitanEdgeEntity<?> entity) {
        final PersistentPropertyAccessor accessor = entity.getPropertyAccessor(domainInstance);

        Object startNode = accessor.getProperty(entity.getStartNode());

        if (startNode == null)
            throw new MappingException("Start node is null: " + domainInstance);

        TitanPersistentEntity<?> startEntity = mappingContext.getPersistentEntity(entity.getStartNode().getTypeInformation());

        Element outVertex = saveOrUpdate(startNode, startEntity);

        Object endNode = accessor.getProperty(entity.getEndNode());

        if (endNode == null)
            throw new MappingException("Start node is null: " + domainInstance);

        TitanPersistentEntity<?> endEntity = mappingContext.getPersistentEntity(entity.getEndNode().getTypeInformation());

        Element inVertex = saveOrUpdate(endNode, endEntity);

        return tx.addEdge((TitanVertex)outVertex, (TitanVertex)inVertex, entity.getLabel());
    }

    public <T> T findOne(Serializable id, TitanPersistentEntity<T> entity) {
        Element titanElement;

        if (entity.isVertex()) {
            assert id instanceof Long;

            titanElement = tx.getVertex(id);
        }
        else {
            titanElement = tx.getEdge(((RelationId)id).getNativeId());
        }

        return titanElement == null ? null : toDomain(titanElement, entity);
    }

    private void saveProperties(@NotNull Object domainInstance,
                                @NotNull final Element titanElement,
                                @NotNull TitanPersistentEntity<?> persistentEntity) {
        final PersistentPropertyAccessor accessor = persistentEntity.getPropertyAccessor(domainInstance);

        persistentEntity.doWithPersistentProperties(new PropertyHandler<TitanPersistentProperty>() {
            @Override public void doWithPersistentProperty(TitanPersistentProperty prop) {
                Object value = accessor.getProperty(prop);

                if (value == null) {
                    titanElement.removeProperty(prop.getName());
                }
                else {
                    titanElement.setProperty(prop.getName(), value);
                }
            }
        });
    }

    private <T> void loadProperties(@NotNull T domainInstance,
                                @NotNull final Element titanElement,
                                @NotNull TitanPersistentEntity<T> entity) {
        final PersistentPropertyAccessor accessor = entity.getPropertyAccessor(domainInstance);

        entity.doWithPersistentProperties(new PropertyHandler<TitanPersistentProperty>() {
            @Override public void doWithPersistentProperty(TitanPersistentProperty prop) {
                Object value = titanElement.getProperty(prop.getName());

                accessor.setProperty(prop, value);
            }
        });

        if (entity instanceof TitanEdgeEntity) {
            TitanEdgeEntity<T> edgeEntity = (TitanEdgeEntity<T>)entity;

            Edge edge = (Edge)titanElement;

            TitanPersistentProperty endNode = edgeEntity.getEndNode();
            TitanPersistentEntity<?> endNodeEntity = mappingContext.getPersistentEntity(endNode.getTypeInformation());

            accessor.setProperty(endNode, toDomain(edge.getVertex(Direction.IN), endNodeEntity));

            TitanPersistentProperty startNode = edgeEntity.getStartNode();
            TitanPersistentEntity<?> startNodeEntity = mappingContext.getPersistentEntity(startNode.getTypeInformation());

            accessor.setProperty(startNode, toDomain(edge.getVertex(Direction.OUT), startNodeEntity));
        }
    }

    public TitanTransaction getTx() {
        return tx;
    }

    public Serializable getId(Object entity) {
        Element data = domainToTitan.get(entity);

        if (data == null)
            return null;

        Object id = data.getId();

        if (id instanceof RelationIdentifier)
            return new RelationId((RelationIdentifier)id);

        return (Long)id;
    }

    public <T> Iterable<T> getAll(TitanPersistentEntity<T> entity) {
        Iterable<? extends Element> elements;

        if (entity.isVertex()) {
            elements = tx.getVertices(CLASS_PROPERTY_NAME, entity.getLabel());
        }
        else {
            elements = tx.getEdges(CLASS_PROPERTY_NAME, entity.getLabel());
        }

        return toDomain(elements, entity);
    }

    public void delete(Serializable id, TitanPersistentEntity<?> entity) {
        Element titanElement;

        if (entity.isVertex()) {
            Vertex vertex = tx.getVertex(id);

            if (vertex == null)
                return;

            tx.removeVertex(vertex);

            titanElement = vertex;
        }
        else {
            Edge edge = tx.getEdge(((RelationId)id).getNativeId());

            if (edge == null)
                return;

            tx.removeEdge(edge);

            titanElement = edge;
        }


        Object domainInstance = titanToDomain.remove(titanElement);

        if (domainInstance != null)
            domainToTitan.remove(domainInstance);

    }

    public void delete(Object instance, TitanPersistentEntity<?> entity) {
        Element titanElement = domainToTitan.remove(instance);

        if (titanElement == null)
            throw new IllegalStateException("Object is not persist");

        titanToDomain.remove(titanElement);

        if (titanElement instanceof Vertex) {
            tx.removeVertex((Vertex)titanElement);
        }
        else {
            tx.removeEdge((Edge)titanElement);
        }
    }

}

package com.evdokimov.testtask.titanMapper;

import com.evdokimov.testtask.titanMapper.annotation.*;
import org.springframework.data.annotation.*;
import org.springframework.data.mapping.*;
import org.springframework.data.mapping.model.*;

import java.beans.*;
import java.lang.reflect.*;

/**
 *
 */
public class TitanPersistentProperty extends AnnotationBasedPersistentProperty<TitanPersistentProperty> {

    private boolean persistentProperty;

    public TitanPersistentProperty(Field field,
                                   PropertyDescriptor propertyDescriptor,
                                   TitanPersistentEntity<?> owner,
                                   SimpleTypeHolder simpleTypeHolder) {
        super(field, propertyDescriptor, owner, simpleTypeHolder);

        persistentProperty = field.isAnnotationPresent(Property.class) || field.isAnnotationPresent(Id.class);

        if (isAssociation()) {
            Class<?> valueType = propertyDescriptor.getPropertyType();

            if (!valueType.isAnnotationPresent(Node.class))
                throw new MappingException("@StartNode and @EndNode fields must reference to node.");
        }
    }

    public boolean isPersistentProperty() {
        return persistentProperty;
    }

    @Override public boolean isAssociation() {
        return !isTransient() && (isAnnotationPresent(StartNode.class) || isAnnotationPresent(EndNode.class));
    }

    @Override protected Association<TitanPersistentProperty> createAssociation() {
        return new Association<>(this, null);
    }
}

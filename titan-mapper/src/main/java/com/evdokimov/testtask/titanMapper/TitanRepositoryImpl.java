package com.evdokimov.testtask.titanMapper;

import com.google.common.collect.*;
import org.jetbrains.annotations.*;

import java.io.*;
import java.util.*;

/**
 *
 */
public class TitanRepositoryImpl<T, ID extends Serializable> implements TitanRepositoryBase<T, ID> {

    protected final TitanManager titanManager;

    protected final TitanPersistentEntity<T> persistentEntity;

    public TitanRepositoryImpl(@NotNull TitanManager titanManager, TitanPersistentEntity<T> persistentEntity) {
        this.titanManager = titanManager;

        this.persistentEntity = persistentEntity;
    }

    @Override public <S extends T> S save(S entity) {
        titanManager.getOrCreateSession().saveOrUpdate(entity, this.persistentEntity);

        return entity;
    }

    @Override public <S extends T> Iterable<S> save(Iterable<S> entities) {
        TitanSession session = titanManager.getOrCreateSession();

        for (S entity : entities) {
            session.saveOrUpdate(entity, this.persistentEntity);
        }

        return entities;
    }

    @Override public T findOne(ID id) {
        return titanManager.getOrCreateSession().findOne(id, persistentEntity);
    }

    @Override public boolean exists(ID id) {
        return findOne(id) != null;
    }

    @Override public Iterable<T> findAll() {
        return titanManager.getOrCreateSession().getAll(persistentEntity);
    }

    @Override public Iterable<T> findAll(Iterable<ID> ids) {
        List<T> res = new ArrayList<>();

        TitanSession session = titanManager.getOrCreateSession();

        for (ID id : ids) {
            T x = session.findOne(id, persistentEntity);

            if (x != null)
                res.add(x);
        }

        return res;
    }

    @Override public long count() {
        return Iterables.size(findAll());
    }

    @Override public void delete(ID id) {
        titanManager.getOrCreateSession().delete(id, persistentEntity);
    }

    @Override public void delete(T entity) {
        titanManager.getOrCreateSession().delete(entity, this.persistentEntity);
    }

    @Override public void delete(Iterable<? extends T> entities) {
        for (T t : entities) {
            titanManager.getOrCreateSession().delete(persistentEntity, this.persistentEntity);
        }
    }

    @Override public void deleteAll() {
        delete(findAll());
    }

    @Override public void commit() {
        titanManager.commit();
    }

    @Override public void rollback() {
        titanManager.rollback();
    }

    @Override public ID getEntityId(@NotNull T entity) {
        return (ID)titanManager.getOrCreateSession().getId(entity);
    }

    @Override public boolean isPersistent(@NotNull T entity) {
        return titanManager.getOrCreateSession().getId(entity) != null;
    }

    @Override public <R> R executeInTransaction(@NotNull TitanCallable<R> callable) {
        return titanManager.executeInTransaction(callable);
    }
}

package com.evdokimov.testtask.titanMapper;

import org.springframework.data.mapping.*;
import org.springframework.data.mapping.model.*;
import org.springframework.data.util.*;

/**
 *
 */
public class TitanPersistentEntity<T> extends BasicPersistentEntity<T, TitanPersistentProperty> {

    public TitanPersistentEntity(TypeInformation<T> information) {
        super(information);
    }

    public boolean isVertex() {
        return true;
    }

    public String getLabel() {
        return getTypeInformation().getType().getSimpleName();
    }

    @Override public void doWithProperties(PropertyHandler<TitanPersistentProperty> handler) {
        super.doWithProperties(handler);
    }

    public void doWithPersistentProperties(final PropertyHandler<TitanPersistentProperty> handler) {
        doWithProperties(new PropertyHandler<TitanPersistentProperty>() {
            @Override public void doWithPersistentProperty(TitanPersistentProperty prop) {
                if (prop.isPersistentProperty()) {
                    handler.doWithPersistentProperty(prop);
                }
            }
        });
    }
}

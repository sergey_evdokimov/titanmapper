package com.evdokimov.testtask.titanMapper;

import com.thinkaurelius.titan.graphdb.relations.*;

import java.io.*;
import java.util.*;

/**
 * Wrapper for com.thinkaurelius.titan.graphdb.relations.RelationIdentifier.
 * RelationIdentifier is not Serializable, but org.springframework.data.repository.Repository require serializable ID.
 */
public class RelationId implements Externalizable {

    private RelationIdentifier nativeId;

    public RelationId(RelationIdentifier nativeId) {
        this.nativeId = nativeId;
    }

    public RelationIdentifier getNativeId() {
        return nativeId;
    }

    @Override public void writeExternal(ObjectOutput out) throws IOException {
        long[] longs = nativeId.getLongRepresentation();

        out.write(longs.length);

        for (long x : longs) {
            out.writeLong(x);
        }
    }

    @Override public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        long[] longs = new long[in.readUnsignedByte()];

        for (int i = 0; i < longs.length; i++) {
            longs[i] = in.readLong();
        }

        nativeId = RelationIdentifier.get(longs);
    }

    @Override public boolean equals(Object o) {
        return o instanceof RelationId && Objects.equals(nativeId, ((RelationId)o).nativeId);
    }

    @Override public int hashCode() {
        return nativeId.hashCode();
    }

    @Override public String toString() {
        return nativeId.toString();
    }
}

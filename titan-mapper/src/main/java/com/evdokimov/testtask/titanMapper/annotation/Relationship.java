package com.evdokimov.testtask.titanMapper.annotation;

import com.thinkaurelius.titan.core.*;
import org.springframework.data.annotation.*;

import java.lang.annotation.*;

/**
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@Persistent
public @interface Relationship {
    Multiplicity multiplicity();
}

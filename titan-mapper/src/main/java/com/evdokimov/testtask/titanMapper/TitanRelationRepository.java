package com.evdokimov.testtask.titanMapper;

import com.tinkerpop.blueprints.*;

/**
 *
 */
public interface TitanRelationRepository<T, Out, In> extends TitanRepositoryBase<T, RelationId> {

    Iterable<T> getOuts(Out out);

    Iterable<T> getIn(In in);

    Iterable<T> getRelations(Direction direction, Object vertexDomain);
}

package com.evdokimov.testtask.titanMapper;

/**
 *
 */
public abstract class TitanRunnable implements TitanCallable<Object> {

    public abstract void run();

    @Override public final Object execute() {
        run();

        return null;
    }
}

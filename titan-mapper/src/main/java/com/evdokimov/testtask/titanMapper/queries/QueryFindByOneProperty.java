package com.evdokimov.testtask.titanMapper.queries;

import com.evdokimov.testtask.titanMapper.*;
import com.thinkaurelius.titan.core.*;
import com.tinkerpop.blueprints.*;
import org.springframework.data.repository.query.*;

import java.util.*;

/**
 *
 */
public class QueryFindByOneProperty implements RepositoryQuery {

    private final TitanManager titanManager;

    private final Class<?> domainType;

    private final String propertyName;

    private final QueryMethod queryMethod;

    public QueryFindByOneProperty(TitanManager titanManager, QueryMethod queryMethod, Class<?> domainType,
                                  String propertyName) {
        this.titanManager = titanManager;
        this.domainType = domainType;
        this.propertyName = propertyName;
        this.queryMethod = queryMethod;
    }

    @Override public Object execute(Object[] parameters) {
        TitanPersistentEntity<?> entity = titanManager.getMappingContext().getPersistentEntity(domainType);

        TitanSession session = titanManager.getOrCreateSession();

        TitanGraphQuery<? extends TitanGraphQuery> query = session.getTx().query()
            .has(propertyName, parameters[0])
            .has(TitanSession.CLASS_PROPERTY_NAME, entity.getLabel());

        return toResult(query.vertices(), session, entity);
    }

    protected Object toResult(Iterable<? extends Element> itr, TitanSession session, TitanPersistentEntity<?> entity) {
        Iterator<? extends Element> iterator = itr.iterator();

        if (!iterator.hasNext())
            return null;

        return session.toDomain(iterator.next(), entity);
    }

    @Override public QueryMethod getQueryMethod() {
        return queryMethod;
    }
}

package com.evdokimov.testtask.titanMapper.config;

import com.evdokimov.testtask.titanMapper.*;
import org.springframework.data.repository.config.*;

/**
 *
 */
public class TitanRepositoryExtension extends RepositoryConfigurationExtensionSupport {
    @Override
    protected String getModulePrefix() {
        return "titan";
    }

    @Override
    public String getRepositoryFactoryClassName() {
        return TitanRepositoryFactory.class.getName();
    }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.evdokimov.testtask.titanMapper;

import com.tinkerpop.blueprints.*;
import org.jetbrains.annotations.*;

/**
 *
 */
public class TitanRelationRepositoryImpl<T, Out, In> extends TitanRepositoryImpl<T, RelationId>
    implements TitanRelationRepository<T, Out, In> {

    public TitanRelationRepositoryImpl(@NotNull TitanManager titanManager,
                                       TitanPersistentEntity<T> persistentEntity) {
        super(titanManager, persistentEntity);
    }

    @Override public Iterable<T> getOuts(Out out) {
        return getRelations(Direction.OUT, out);
    }

    @Override public Iterable<T> getIn(In in) {
        return getRelations(Direction.IN, in);
    }

    public Iterable<T> getRelations(Direction direction, Object vertexDomain) {
        TitanSession session = titanManager.getOrCreateSession();

        Element element = session.toTitanElement(vertexDomain);
        if (element == null)
            throw new IllegalArgumentException("Object is not persisted: " + vertexDomain);

        Iterable<Edge> edges = ((Vertex)element).getEdges(direction, persistentEntity.getLabel());

        return session.toDomain(edges, persistentEntity);
    }
}

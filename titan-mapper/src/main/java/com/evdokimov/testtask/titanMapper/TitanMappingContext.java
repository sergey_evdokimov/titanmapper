package com.evdokimov.testtask.titanMapper;

import com.evdokimov.testtask.titanMapper.annotation.*;
import org.springframework.data.mapping.context.*;
import org.springframework.data.mapping.model.*;
import org.springframework.data.util.*;

import java.beans.*;
import java.lang.reflect.*;

/**
 *
 */
public class TitanMappingContext extends AbstractMappingContext<TitanPersistentEntity<?>, TitanPersistentProperty> {

    @Override protected <T> TitanPersistentEntity createPersistentEntity(TypeInformation<T> typeInformation) {
        final Class<T> type = typeInformation.getType();

        if (type.isAnnotationPresent(Node.class))
            return new TitanPersistentEntity<T>(typeInformation);

        if (type.isAnnotationPresent(Relationship.class))
            return new TitanEdgeEntity<>(typeInformation);

        throw new MappingException(type.getSimpleName() + " must be annotated @" + Node.class.getSimpleName() +
            " or @" + Relationship.class.getSimpleName());
    }

    @Override protected TitanPersistentProperty createPersistentProperty(Field field,
                                                                         PropertyDescriptor descriptor,
                                                                         TitanPersistentEntity owner,
                                                                         SimpleTypeHolder simpleTypeHolder) {
        return new TitanPersistentProperty(field, descriptor, owner, simpleTypeHolder);
    }
}

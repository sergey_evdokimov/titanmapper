package com.evdokimov.testtask.titanMapper;

import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.*;
import com.thinkaurelius.titan.core.schema.*;
import com.tinkerpop.blueprints.*;
import org.jetbrains.annotations.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.*;

/**
 *
 */
public class TitanManager implements InitializingBean {
    @Autowired
    private TitanGraph graph;

    private final ThreadLocal<TitanSession> sessions = new ThreadLocal<>();

    private TitanMappingContext mappingContext = new TitanMappingContext();

    public void setGraph(TitanGraph graph) {
        this.graph = graph;
    }

    public TitanGraph getGraph() {
        return graph;
    }

    public void commit() {
        TitanSession session = sessions.get();

        if (session != null) {
            sessions.remove();

            session.getTx().commit();
        }
    }

    public void rollback() {
        TitanSession session = sessions.get();

        if (session != null) {
            sessions.remove();

            session.getTx().rollback();
        }
    }

    public TitanSession getOrCreateSession() {
        TitanSession session = sessions.get();

        if (session == null || !session.getTx().isOpen()) {
            session = new TitanSession(graph.newTransaction(), mappingContext);

            sessions.set(session);
        }

        return session;
    }

    public <R> R executeInTransaction(@NotNull TitanCallable<R> callable) {
        TitanSession oldSession = sessions.get();

        try {
            TitanSession session = new TitanSession(graph.newTransaction(), mappingContext);

            sessions.set(session);

            try {
                R res = callable.execute();

                sessions.remove();

                session.getTx().commit();

                return res;
            }
            finally {
                TitanSession s = sessions.get();

                if (s != null)
                    s.getTx().rollback();
            }
        }
        finally {
            sessions.set(oldSession);
        }
    }

    public TitanMappingContext getMappingContext() {
        return mappingContext;
    }

    @Override public void afterPropertiesSet() throws Exception {
        mappingContext = new TitanMappingContext();

        mappingContext.initialize();
    }

    public void updateGraphSchema(Class<?> ... domainClasses) {
        TitanManagement mgmt = graph.getManagementSystem();

        if (mgmt.getPropertyKey(TitanSession.CLASS_PROPERTY_NAME) == null) {
            PropertyKey classPropertyKey = mgmt.makePropertyKey("class").dataType(String.class).make();

            mgmt.buildIndex("classIndex", Vertex.class).addKey(classPropertyKey).buildCompositeIndex();
        }

        for (Class<?> domainClass : domainClasses) {
            TitanPersistentEntity<?> entity = mappingContext.getPersistentEntity(domainClass);

            if (entity instanceof TitanEdgeEntity) {
                TitanEdgeEntity<?> edgeEntity = (TitanEdgeEntity<?>)entity;

                if (mgmt.getEdgeLabel(entity.getLabel()) == null) {
                    mgmt.makeEdgeLabel(entity.getLabel()).multiplicity(edgeEntity.getMultiplicity()).make();
                }
            }
        }

        mgmt.commit();
    }

}

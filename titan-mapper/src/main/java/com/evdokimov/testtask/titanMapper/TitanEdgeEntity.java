package com.evdokimov.testtask.titanMapper;

import com.evdokimov.testtask.titanMapper.annotation.*;
import com.thinkaurelius.titan.core.*;
import org.jetbrains.annotations.*;
import org.springframework.data.util.*;

/**
 *
 */
public class TitanEdgeEntity<T> extends TitanPersistentEntity<T> {

    private Multiplicity multiplicity;

    public TitanEdgeEntity(TypeInformation<T> information) {
        super(information);

        Relationship annotation = information.getType().getAnnotation(Relationship.class);

        multiplicity = annotation.multiplicity();
    }

    @Override public boolean isVertex() {
        return false;
    }

    @NotNull
    public TitanPersistentProperty getStartNode() {
        return getPersistentProperty(StartNode.class);
    }

    @NotNull
    public TitanPersistentProperty getEndNode() {
        return getPersistentProperty(EndNode.class);
    }

    @NotNull
    public Multiplicity getMultiplicity() {
        return multiplicity;
    }
}

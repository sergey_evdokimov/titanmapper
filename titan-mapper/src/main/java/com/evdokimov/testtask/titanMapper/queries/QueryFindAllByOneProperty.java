package com.evdokimov.testtask.titanMapper.queries;

import com.evdokimov.testtask.titanMapper.*;
import com.tinkerpop.blueprints.*;
import org.springframework.data.repository.query.*;

/**
 *
 */
public class QueryFindAllByOneProperty extends QueryFindByOneProperty {

    public QueryFindAllByOneProperty(TitanManager titanManager,
                                     QueryMethod queryMethod,
                                     Class<?> domainType, String propertyName) {
        super(titanManager, queryMethod, domainType, propertyName);
    }

    @Override
    protected Object toResult(Iterable<? extends Element> itr, TitanSession session, TitanPersistentEntity<?> entity) {
        return session.toDomain(itr, entity);
    }
}
